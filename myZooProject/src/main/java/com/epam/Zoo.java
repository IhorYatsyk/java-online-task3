package com.epam;
    class Animal {
        void sleep() {
            System.out.println("I sleep");
        }
        void eatAnything() {
            System.out.println("I eat anything");
        }
    }
    class Fish extends Animal {
        void swim() {
            System.out.println("I swim");
        }
    }
    class Bird extends Animal {
        void fly() {
            System.out.println("I fly");
        }
    }
    class Parrot extends Bird {
        void eatPlants() {
            System.out.println("I eat plants");
        }
    }
    class Eagle extends Bird {
        void eatMeat() {
            System.out.println("I eat meat");
        }
    }
    class Solution {
        public static void main(String[] args) {
            Bird bird = new Bird();
            bird.fly();
            bird.sleep();
        }
    }