package com.epam;

public class Shape {
    }
class Rectangle extends Shape{
    void angles(){
        System.out.println("I have four angles");
    }
    void red(){
        System.out.println("I am red rectangle");
    }
    void blue(){
        System.out.println("I am blue rectangle");
    }
    void yellow(){
        System.out.println("I am yellow rectangle");
    }
}
class Circle extends Shape{
    void angles(){
        System.out.println("I have no angles");
    }
    void red(){
        System.out.println("I am red circle");
    }
    void blue(){
        System.out.println("I am blue circle");
    }
    void green(){
        System.out.println("I am green triangle");
    }
}
class Triangle extends Shape{
        void angles(){
        System.out.println("I have three angles");
        }
        void orange(){
        System.out.println("I am orange circle");
        }
        void purple(){
        System.out.println("I am purple circle");
        }
        void grey(){
        System.out.println("I am grey triangle");
        }
}
class Solution {
    public static void main(String[] args) {
        Circle circle = new Circle();
        circle.angles();
        circle.blue();
        circle.red();
        circle.green();
    }
}